package it.demo.esercitazione.jpa.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "CARS")
public class CarEntity implements JpaEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "car_id")
    private Long id;

    private String company;

    private String name;

    /*1.Con @ManyToOne indichiamo a JPA che CarEntity � in relazione N:1 con UserEntity. 
     * Quindi il campo userEntity lato DB sar� una FK.*/
    @ManyToOne
    
    /*2.Con @JoinColumn(name = "user_id") indichiamo a JPA qual � il nome della colonna che fa da FK. 
     * Siccome sia nella tabella CAR che nella tabella USERS la colonna che fa da FK ha lo stesso nome, basta usare name = "user_id", altrimenti, 
     * se la tabella USERS avesse come nome della colonna PK "id", 
     * saremmo stati costretti a scrivere in questo modo @JoinColumn(name = "user_id", referencedColumnName = "id").*/
    @JoinColumn(name = "user_id")
    private UserEntity userEntity;

  //getters, setters, equals, hashcode
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public UserEntity getUserEntity() {
		return userEntity;
	}

	public void setUserEntity(UserEntity userEntity) {
		this.userEntity = userEntity;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((company == null) ? 0 : company.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((userEntity == null) ? 0 : userEntity.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CarEntity other = (CarEntity) obj;
		if (company == null) {
			if (other.company != null)
				return false;
		} else if (!company.equals(other.company))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (userEntity == null) {
			if (other.userEntity != null)
				return false;
		} else if (!userEntity.equals(other.userEntity))
			return false;
		return true;
	}
    
    
    
}
