package it.demo.esercitazione.jpa.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

//1.Con @Entity stiamo indicando a JPA che questa classe Java mappa una tabella del database.
@Entity

/*2.Con @Table(name = "USERS") indichiamo a JPA che il nome della tabella mappata � USERS. 
 * Senza questa annotation, cercherebbe una tabella con lo stesso nome della classe Java.*/
@Table(name = "USERS")
public class UserEntity implements JpaEntity {
	
	//3.Con @Id indichiamo che il campo annotato � una primary key.
	@Id

	/*4.Con @GeneratedValue(strategy = GenerationType.IDENTITY) indichiamo che l'id � una IDENTITY 
	 * (cio� � un campo che viene auto-incrementato dal DBMS).*/
	@GeneratedValue(strategy = GenerationType.IDENTITY)

	/*5.Con @Column(name = "user_id"), indichiamo a JPA che questo campo Java mappa la colonna user_id. 
	 * Senza questa annotation, JPA cercherebbe di mappare una colonna con lo stesso nome del campo Java*/	
	@Column(name = "user_id")
	private Long id;

	private String name;

	private String surname;

	private String code;

	/*6.Con @OneToMany(mappedBy = "userEntity") stiamo dicendo a JPA che questa tabella � in relazione 1:N con la classe CarEntity che mappa la tabella CARS. 
	 * Inoltre, con mappedBy indichiamo a JPA che CarEntity ha un attributo chiamato userEntity per mappare la relazione inversa N:1.*/
	@OneToMany(mappedBy = "userEntity")
	private Set<CarEntity> cars;

	/*7.Con @OneToOne(mappedBy = "userEntity", cascade = CascadeType.ALL, orphanRemoval = true) stiamo indicando a JPA che c'� una relazione 1:1 con la classe ContactEntity che mappa la tabella CONTACTS. 
	 * Il discorso del mappedBy � uguale al punto 6. Inoltre, con cascade = CascadeType.ALL, indichiamo che � attivo il cascade su tutte le operazioni di INSERT, UPDATE, DELETE
	 * (non � obbligatorio avere il cascade lato db). Infine, con orphanRemoval = true indichiamo che se un figlio, ContactEntity, rimane "orfano" del padre, UserEntity, 
	 * (ovvero ha foreign key null), deve essere cancellato automaticamente.*/
	@OneToOne(mappedBy = "userEntity", cascade = CascadeType.ALL, orphanRemoval = true)
	
	//8.Con @PrimaryKeyJoinColumn, indichiamo a JPA che la tabella CONTACTS ha come chiave primaria la stessa della tabella USERS.
	@PrimaryKeyJoinColumn
	private ContactEntity contactEntity;

	/*9.Con @ManyToMany(mappedBy = "authors") indichiamo a JPA che c'� una relazione N:N con la classe ArticleEntity e 
	 * che ArticleEntity ha una collezione di UserEntity con nome authors.*/ 
	@ManyToMany(mappedBy = "authors")
	private Set<ArticleEntity> articles;

	public void addCar(CarEntity carEntity) {
		carEntity.setUserEntity(this);
		if(this.cars == null) {
			this.cars = new HashSet<CarEntity>();
		}
		this.cars.add(carEntity);
	}

	public void removeCar(CarEntity carEntity) {
		this.cars.remove(carEntity);
		carEntity.setUserEntity(null);
	}

	public void addArticle(ArticleEntity articleEntity) {
		if(articleEntity.getAuthors() == null) {
			articleEntity.setAuthors(new HashSet<UserEntity>());
		}

		if(this.articles == null) {
			this.articles = new HashSet<ArticleEntity>();
		}

		articleEntity.getAuthors().add(this);
		this.articles.add(articleEntity);
	}

	public void removeArticle(ArticleEntity articleEntity) {
		this.articles.remove(articleEntity);
		articleEntity.getAuthors().remove(this);
	}

	//getters, setters, equals, hashcode
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Set<CarEntity> getCars() {
		return cars;
	}

	public void setCars(Set<CarEntity> cars) {
		this.cars = cars;
	}

	public ContactEntity getContactEntity() {
		return contactEntity;
	}

	public void setContactEntity(ContactEntity contactEntity) {
		this.contactEntity = contactEntity;
	}

	public Set<ArticleEntity> getArticles() {
		return articles;
	}

	public void setArticles(Set<ArticleEntity> articles) {
		this.articles = articles;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((surname == null) ? 0 : surname.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserEntity other = (UserEntity) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (surname == null) {
			if (other.surname != null)
				return false;
		} else if (!surname.equals(other.surname))
			return false;
		return true;
	}



}

