package it.demo.esercitazione.jpa.daos.impl;

import it.demo.esercitazione.jpa.daos.CarDao;
import it.demo.esercitazione.jpa.entities.CarEntity;

public class CarDaoImpl extends JpaDaoImpl<CarEntity, Long> implements CarDao {

    private static CarDao carDao;

    private CarDaoImpl(){}

    public static CarDao getInstance() {
        if(carDao == null) {
            carDao = new CarDaoImpl();
        }
        return carDao;
    }
}
