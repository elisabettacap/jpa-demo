package it.demo.esercitazione.jpa.daos;

import it.demo.esercitazione.jpa.entities.CarEntity;

public interface CarDao extends JpaDao<CarEntity, Long> {
}
