package it.demo.esercitazione.jpa.daos;

import java.util.Collection;

import it.demo.esercitazione.jpa.entities.ArticleEntity;
import it.demo.esercitazione.jpa.entities.UserEntity;

public interface ArticleDao extends JpaDao<ArticleEntity, Long> {

    Collection<UserEntity> findAuthorsById(Long articleId);
}
