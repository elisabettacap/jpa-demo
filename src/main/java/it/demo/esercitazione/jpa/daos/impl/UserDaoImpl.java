package it.demo.esercitazione.jpa.daos.impl;

import it.demo.esercitazione.jpa.daos.UserDao;
import it.demo.esercitazione.jpa.entities.UserEntity;

public class UserDaoImpl extends JpaDaoImpl<UserEntity, Long> implements UserDao {

    private static UserDaoImpl userDao;

    private UserDaoImpl() {}

    public static UserDao getInstance() {
        if(userDao == null) {
            userDao = new UserDaoImpl();
        }

        return userDao;
    }

}
