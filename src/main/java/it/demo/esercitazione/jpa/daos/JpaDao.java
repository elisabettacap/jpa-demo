package it.demo.esercitazione.jpa.daos;

import java.util.Collection;

import it.demo.esercitazione.jpa.entities.JpaEntity;

public interface JpaDao<T extends JpaEntity, ID> {

    T findById(ID id);

    Collection<T> findAll();

    T save(T entity);

    void delete(T entity);

    void clear();
}