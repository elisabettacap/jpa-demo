package it.demo.esercitazione.jpa.daos.impl;

import java.util.Collection;

import it.demo.esercitazione.jpa.daos.ArticleDao;
import it.demo.esercitazione.jpa.entities.ArticleEntity;
import it.demo.esercitazione.jpa.entities.UserEntity;

public class ArticleDaoImpl extends JpaDaoImpl<ArticleEntity, Long> implements ArticleDao {

    private static ArticleDao articleDao;

    private ArticleDaoImpl() {}

    public static ArticleDao getInstance(){
        if(articleDao == null) {
            articleDao = new ArticleDaoImpl();
        }
        return articleDao;
    }

    @Override
    public Collection<UserEntity> findAuthorsById(Long articleId) {
        ArticleEntity articleEntity = findById(articleId);
        return articleEntity.getAuthors();
    }
}
