package it.demo.esercitazione.jpa.daos;

import it.demo.esercitazione.jpa.entities.UserEntity;

public interface UserDao extends JpaDao<UserEntity, Long> {
}
