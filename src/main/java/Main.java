import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import it.demo.esercitazione.jpa.config.JpaConfig;
import it.demo.esercitazione.jpa.daos.UserDao;

public class Main {


    public static void main(String... args) {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(JpaConfig.class);
        UserDao bean = ctx.getBean(UserDao.class);
        System.out.println(bean);
    }
}
